package ru.bokhan.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.bokhan.tm.endpoint.*;

@Configuration
@ComponentScan("ru.bokhan.tm")
public class ClientConfiguration {

    @Bean
    @NotNull
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Bean
    @NotNull
    public UserEndpoint userEndpoint(
            @NotNull @Autowired final UserEndpointService userEndpointService
    ) {
        return userEndpointService.getUserEndpointPort();
    }

    @Bean
    @NotNull
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @Bean
    @NotNull
    public SessionEndpoint sessionEndpoint(
            @NotNull @Autowired final SessionEndpointService sessionEndpointService
    ) {
        return sessionEndpointService.getSessionEndpointPort();
    }

    @Bean
    @NotNull
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint(
            @NotNull @Autowired final TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint(
            @NotNull @Autowired final ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public ServerInfoEndpointService serverInfoEndpointService() {
        return new ServerInfoEndpointService();
    }

    @Bean
    @NotNull
    public ServerInfoEndpoint serverInfoEndpoint(
            @NotNull @Autowired final ServerInfoEndpointService serverInfoEndpointService
    ) {
        return serverInfoEndpointService.getServerInfoEndpointPort();
    }

    @Bean
    @NotNull
    public DomainEndpointService domainEndpointService() {
        return new DomainEndpointService();
    }

    @Bean
    @NotNull
    public DomainEndpoint domainEndpoint(
            @NotNull @Autowired final DomainEndpointService domainEndpointService
    ) {
        return domainEndpointService.getDomainEndpointPort();
    }

}
