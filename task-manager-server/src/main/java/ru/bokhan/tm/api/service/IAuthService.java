package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    void login(@Nullable String login, @Nullable String password);

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
