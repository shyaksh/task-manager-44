package ru.bokhan.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.bokhan.tm.api.endpoint.IUserEndpoint;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.security.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
@NoArgsConstructor
@AllArgsConstructor
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    IUserService userService;

    @Override
    @WebMethod
    public void createUser(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        userService.create(login, password);
    }

    @Override
    @WebMethod
    public void createUserWithEmail(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        userService.create(login, password, email);
    }

    @Override
    @WebMethod
    public void registerUserWithEmail(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email
    ) {
        userService.create(login, password, email);
    }

    @Override
    @WebMethod
    public void createUserWithRole(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "role") @Nullable final Role role
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        userService.create(login, password, role);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO findUserById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return userService.findById(id);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO findUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return userService.findByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUserById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        userService.removeById(id);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        userService.removeByLogin(login);
    }

    @Override
    @WebMethod
    public void updateUser(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName") @Nullable final String middleName,
            @WebParam(name = "email") @Nullable final String email
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        userService
                .updateById(session.getUserId(), login, firstName, lastName, middleName, email);
    }

    @Override
    @WebMethod
    public void updateUserById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName") @Nullable final String middleName,
            @WebParam(name = "email") @Nullable final String email
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        userService
                .updateById(id, login, firstName, lastName, middleName, email);
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "password") @Nullable final String password
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        userService.updatePasswordById(session.getUserId(), password);
    }

    @Override
    @WebMethod
    public void updateUserPasswordById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "password") @Nullable final String password
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        userService.updatePasswordById(id, password);
    }

    @Override
    @WebMethod
    public void lockUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        userService.lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        userService.unlockUserByLogin(login);
    }

    @Override
    @NotNull
    @WebMethod
    public List<UserDTO> findUserAll(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return userService.findAll();
    }

    @Override
    @WebMethod
    public void removeUserAll(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        userService.clear();
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "user") @Nullable final UserDTO user
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        userService.remove(user);
    }

}
