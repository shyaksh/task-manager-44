package ru.bokhan.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.dto.ProjectDTO;

import java.util.List;

public interface ProjectRepositoryDTO extends AbstractRepositoryDTO<ProjectDTO> {

    ProjectDTO findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    ProjectDTO findByUserIdAndName(@NotNull final String userId, @NotNull final String name);

    List<ProjectDTO> findByUserId(@NotNull final String userId);

}