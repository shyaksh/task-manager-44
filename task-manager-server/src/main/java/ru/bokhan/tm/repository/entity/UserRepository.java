package ru.bokhan.tm.repository.entity;

import ru.bokhan.tm.entity.User;

public interface UserRepository extends AbstractRepository<User> {

    void deleteByLogin(String login);

}