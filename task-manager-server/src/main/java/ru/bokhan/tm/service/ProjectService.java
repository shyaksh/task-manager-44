package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.api.service.IProjectService;
import ru.bokhan.tm.dto.ProjectDTO;
import ru.bokhan.tm.entity.Project;
import ru.bokhan.tm.exception.empty.EmptyDescriptionException;
import ru.bokhan.tm.exception.empty.EmptyIdException;
import ru.bokhan.tm.exception.empty.EmptyNameException;
import ru.bokhan.tm.exception.empty.EmptyUserIdException;
import ru.bokhan.tm.exception.incorrect.IncorrectIdException;
import ru.bokhan.tm.exception.incorrect.IncorrectIndexException;
import ru.bokhan.tm.repository.dto.ProjectRepositoryDTO;
import ru.bokhan.tm.repository.entity.ProjectRepository;

import java.util.List;

@Service
public class ProjectService extends AbstractService<ProjectDTO, Project> implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepositoryDTO dtoRepository;

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        save(project);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        save(project);
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final ProjectDTO projectDTO) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectDTO == null) throw new EmptyUserIdException();
        projectDTO.setUserId(userId);
        save(projectDTO);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return dtoRepository.findByUserId(userId);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.deleteByUserId(userId);
    }

    @Nullable
    @Override
    public ProjectDTO findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return dtoRepository.findByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public ProjectDTO findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final List<ProjectDTO> list = findAll(userId);
        if (index >= list.size()) return null;
        return list.get(index);
    }

    @Nullable
    @Override
    public ProjectDTO findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return dtoRepository.findByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final ProjectDTO project = findByIndex(userId, index);
        if (project == null) return;
        repository.deleteById(project.getId());
    }

    @Override
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        repository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ProjectDTO project = findById(userId, id);
        if (project == null) throw new IncorrectIdException();
        project.setName(name);
        project.setDescription(description);
        save(project);
    }

    @Override
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ProjectDTO project = findByIndex(userId, index);
        if (project == null) throw new IncorrectIndexException();
        project.setName(name);
        project.setDescription(description);
        save(project);
    }

}