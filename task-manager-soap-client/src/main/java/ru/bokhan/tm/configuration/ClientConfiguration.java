package ru.bokhan.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.bokhan.tm.endpoint.soap.*;

@Configuration
@ComponentScan("ru.bokhan.tm")
public class ClientConfiguration {

    @Bean
    @NotNull
    public AuthenticationEndpointService authenticationEndpointService() {
        return new AuthenticationEndpointService();
    }

    @Bean
    @NotNull
    public AuthenticationEndpoint authenticationEndpoint(
            @NotNull @Autowired final AuthenticationEndpointService authenticationEndpointService
    ) {
        return authenticationEndpointService.getAuthenticationEndpointPort();
    }

    @Bean
    @NotNull
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint(
            @NotNull @Autowired final TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint(
            @NotNull @Autowired final ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

}
