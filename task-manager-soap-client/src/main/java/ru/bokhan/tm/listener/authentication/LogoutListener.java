package ru.bokhan.tm.listener.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.service.ISessionService;
import ru.bokhan.tm.endpoint.soap.AuthenticationEndpoint;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.exception.security.AccessDeniedException;
import ru.bokhan.tm.listener.AbstractListener;

@Component
public final class LogoutListener extends AbstractListener {

    @NotNull
    @Autowired
    private AuthenticationEndpoint authenticationEndpoint;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String command() {
        return "logout";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Log out.";
    }

    @Override
    @EventListener(condition = "@logoutListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[LOGOUT]");
        if (authenticationEndpoint.logout().isSuccess()) {
            sessionService.clear();
            System.out.println("[OK:]");
        } else {
            System.out.println("[FAIL]");
            throw new AccessDeniedException();
        }

    }

}