package ru.bokhan.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.service.ISessionService;
import ru.bokhan.tm.endpoint.soap.TaskDto;
import ru.bokhan.tm.endpoint.soap.TaskEndpoint;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.listener.AbstractListener;

import java.util.List;

@Component
public final class TaskListListener extends AbstractListener {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    @EventListener(condition = "@taskListListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        sessionService.insertTo(taskEndpoint);
        System.out.println("[LIST TASKS]");
        @NotNull final List<TaskDto> tasks = taskEndpoint.findTaskAll();
        for (@Nullable final TaskDto task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

}
