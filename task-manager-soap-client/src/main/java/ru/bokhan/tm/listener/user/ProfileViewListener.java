package ru.bokhan.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.service.ISessionService;
import ru.bokhan.tm.endpoint.soap.AuthenticationEndpoint;
import ru.bokhan.tm.endpoint.soap.UserDto;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.listener.AbstractListener;

@Component
public final class ProfileViewListener extends AbstractListener {

    @NotNull
    @Autowired
    private AuthenticationEndpoint authenticationEndpoint;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String command() {
        return "profile-view";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show my profile";
    }


    @Override
    @EventListener(condition = "@profileViewListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        sessionService.insertTo(authenticationEndpoint);
        System.out.println("[SHOW PROFILE]");
        @Nullable final UserDto user = authenticationEndpoint.profile();
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getId());
    }

}
