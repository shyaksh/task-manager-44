package ru.bokhan.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.bokhan.tm.api.service.IProjectService;
import ru.bokhan.tm.dto.CustomUser;
import ru.bokhan.tm.dto.ProjectDto;

import java.util.List;

@Controller
public class ProjectsController {

    @Autowired
    private IProjectService service;

    @GetMapping("/projects")
    public ModelAndView index(
            @NotNull @AuthenticationPrincipal CustomUser user
    ) {
        @NotNull final List<ProjectDto> projects = service.findAllByUserId(user.getUserId());
        return new ModelAndView("project-list", "projects", projects);
    }

}