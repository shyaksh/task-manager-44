package ru.bokhan.tm.dto;

import org.jetbrains.annotations.NotNull;

public class Result {

    private boolean success = true;

    public String message = "";

    @NotNull
    public Result(final boolean success) {
        this.success = success;
    }

    @NotNull
    public Result(@NotNull final Exception e) {
        this.success = false;
        this.message = e.getMessage();
    }

    public Result() {

    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(final boolean success) {
        this.success = success;
    }

}
