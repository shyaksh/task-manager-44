package ru.bokhan.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.dto.ProjectDto;
import ru.bokhan.tm.repository.dto.ProjectDtoRepository;
import ru.bokhan.tm.repository.entity.ProjectRepository;
import ru.bokhan.tm.util.SecurityUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class ProjectEndpoint {

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @WebMethod
    public List<ProjectDto> findProjectAll() {
        return projectDtoRepository.findAllByUserId(SecurityUtil.getUserId());
    }

    @NotNull
    @WebMethod
    public ProjectDto saveProject(@NotNull @WebParam(name = "project") final ProjectDto project) {
        project.setUserId(SecurityUtil.getUserId());
        return projectDtoRepository.save(project);
    }

    @Nullable
    @WebMethod
    public ProjectDto findProjectById(@NotNull @WebParam(name = "id") final String id) {
        return projectDtoRepository.findByUserIdAndId(SecurityUtil.getUserId(), id);

    }

    @WebMethod
    public boolean existsProjectById(@NotNull @WebParam(name = "id") final String id) {
        return projectDtoRepository.existsByUserIdAndId(SecurityUtil.getUserId(), id);
    }

    @WebMethod
    public long countProject() {
        return projectDtoRepository.countByUserId(SecurityUtil.getUserId());
    }

    @WebMethod
    public void deleteProjectById(@NotNull @WebParam(name = "id") final String id) {
        projectRepository.deleteByUserIdAndId(SecurityUtil.getUserId(), id);
    }

    @WebMethod
    public void deleteProjectAll() {
        projectRepository.deleteAllByUserId(SecurityUtil.getUserId());
    }

}
