package ru.bokhan.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.dto.RoleDto;
import ru.bokhan.tm.enumerated.RoleType;

import java.util.Arrays;
import java.util.List;

import static ru.bokhan.tm.constant.UserTestData.*;

@UtilityClass
public class RoleTestData {

    @NotNull
    public final static RoleDto USER1_ROLE = new RoleDto(USER1.getId());

    @NotNull
    public final static RoleDto NEW_USER_ROLE = new RoleDto(NEW_USER.getId());

    @NotNull
    public final static RoleDto ADMIN1_ROLE_USER = new RoleDto(ADMIN1.getId());

    @NotNull
    public final static RoleDto ADMIN1_ROLE_ADMIN = new RoleDto(ADMIN1.getId(), RoleType.ADMINISTRATOR);

    @NotNull
    public final static List<RoleDto> ROLE_LIST = Arrays.asList(
            USER1_ROLE, ADMIN1_ROLE_ADMIN, ADMIN1_ROLE_USER
    );

}
