package ru.bokhan.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.dto.TaskDto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ru.bokhan.tm.constant.ProjectTestData.*;
import static ru.bokhan.tm.constant.UserTestData.*;

@UtilityClass
public final class TaskTestData {

    @NotNull
    public final static TaskDto USER1_TASK1 = new TaskDto();

    @NotNull
    public final static TaskDto USER1_TASK2 = new TaskDto();

    @NotNull
    public final static TaskDto USER1_NEW_TASK = new TaskDto();

    @NotNull
    public final static TaskDto ADMIN1_TASK1 = new TaskDto();

    @NotNull
    public final static TaskDto ADMIN1_TASK2 = new TaskDto();

    @NotNull
    public final static TaskDto NEW_USER_TASK1 = new TaskDto();

    @NotNull
    public final static List<TaskDto> USER1_TASK_LIST = Arrays.asList(USER1_TASK1, USER1_TASK2);

    @NotNull
    public final static List<TaskDto> ADMIN1_TASK_LIST = Arrays.asList(ADMIN1_TASK1, ADMIN1_TASK2);

    @NotNull
    public final static List<TaskDto> TASK_LIST = new ArrayList<>();

    @NotNull
    public final static List<TaskDto> NEW_USER_TASK_LIST = Collections.singletonList(NEW_USER_TASK1);

    static {
        TASK_LIST.addAll(USER1_TASK_LIST);
        TASK_LIST.addAll(ADMIN1_TASK_LIST);
    }

    public static void initData() {
        USER1_TASK_LIST.forEach(task -> task.setUserId(USER1.getId()));
        USER1_TASK_LIST.forEach(task -> task.setProjectId(USER1_PROJECT1.getId()));
        ADMIN1_TASK_LIST.forEach(task -> task.setUserId(ADMIN1.getId()));
        ADMIN1_TASK_LIST.forEach(task -> task.setProjectId(ADMIN1_PROJECT1.getId()));

        for (int i = 0; i < TASK_LIST.size(); i++) {
            @NotNull final TaskDto task = TASK_LIST.get(i);
            task.setName("task-" + i);
            task.setDescription("description of task-" + i);
        }

        NEW_USER_TASK1.setName("New-Task");
        NEW_USER_TASK1.setUserId(NEW_USER.getId());
        NEW_USER_TASK1.setUserId(NEW_USER.getId());
        NEW_USER_TASK1.setProjectId(NEW_USER_PROJECT1.getId());

        USER1_NEW_TASK.setName("User-1-New-Task");
        USER1_NEW_TASK.setUserId(USER1.getId());
        USER1_NEW_TASK.setUserId(USER1.getId());
        USER1_NEW_TASK.setProjectId(USER1_PROJECT1.getId());
    }

}
