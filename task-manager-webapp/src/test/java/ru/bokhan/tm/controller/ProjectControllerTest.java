package ru.bokhan.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.bokhan.tm.AbstractMockMvcTest;
import ru.bokhan.tm.dto.ProjectDto;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static ru.bokhan.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.bokhan.tm.constant.UserTestData.USER1;

public class ProjectControllerTest extends AbstractMockMvcTest {

    @NotNull
    private final String baseUrl = "/project";

    @Test
    public void create() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get(baseUrl + "/create"))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(view().name(containsString("redirect:/project/edit/")));
    }

    @Test
    public void delete() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get(baseUrl + "/delete/" + USER1_PROJECT1.getId()))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/projects"));
        Assert.assertFalse(projectService.existsByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void editGet() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get(baseUrl + "/edit/" + USER1_PROJECT1.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("project-edit"));
    }

    @Test
    public void editPost() throws Exception {
        USER1_PROJECT1.setName("edited");
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(baseUrl + "/edit/" + USER1_PROJECT1.getId())
                        .flashAttr("project", USER1_PROJECT1)
        )
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/projects"));

        @Nullable final ProjectDto actual = projectService.findByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertNotNull(actual);
        Assert.assertEquals("edited", actual.getName());
    }

}