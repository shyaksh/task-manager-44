package ru.bokhan.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.bokhan.tm.AbstractMockMvcTest;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class ProjectsControllerTest extends AbstractMockMvcTest {

    @NotNull
    private final String baseUrl = "/projects";

    @Test
    public void index() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get(baseUrl))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("project-list"));
    }

}