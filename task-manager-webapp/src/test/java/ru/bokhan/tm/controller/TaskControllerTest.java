package ru.bokhan.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.bokhan.tm.AbstractMockMvcTest;
import ru.bokhan.tm.dto.TaskDto;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static ru.bokhan.tm.constant.TaskTestData.USER1_TASK1;
import static ru.bokhan.tm.constant.UserTestData.USER1;

public class TaskControllerTest extends AbstractMockMvcTest {

    @NotNull
    private final String baseUrl = "/task";

    @Test
    public void create() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get(baseUrl + "/create"))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(view().name(containsString("redirect:/task/edit/")));
    }

    @Test
    public void delete() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get(baseUrl + "/delete/" + USER1_TASK1.getId()))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/tasks"));
        Assert.assertFalse(taskService.existsByUserIdAndId(USER1.getId(), USER1_TASK1.getId()));
    }

    @Test
    public void editGet() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get(baseUrl + "/edit/" + USER1_TASK1.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("task-edit"));
    }

    @Test
    public void editPost() throws Exception {
        USER1_TASK1.setName("edited");
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(baseUrl + "/edit/" + USER1_TASK1.getId())
                        .flashAttr("task", USER1_TASK1)
        )
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/tasks"));

        @Nullable final TaskDto actual = taskService.findByUserIdAndId(USER1.getId(), USER1_TASK1.getId());
        Assert.assertNotNull(actual);
        Assert.assertEquals("edited", actual.getName());
    }

}